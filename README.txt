Philip Mocz
Spring 2014

MHD with CR pressure
Fixed grid, Constrained Transport Code in Matlab

Astronomy 253 final project

analysis/             scripts to analyze output
driver/               driver files to run simulations
  Driver.m              main driver file
  Driver_upstream       test driver -- simplified simulation of upstream only
FVCT_2D/              main code