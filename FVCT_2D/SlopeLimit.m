function grad_phi = SlopeLimit(phi, grad_phi)
% slope limit the gradients

Globals2D;

alpha = ones(size(phi));

phi_max = phi;
phi_max = max(phi_max,circshift(phi,Right));
phi_max = max(phi_max,circshift(phi,Left));
phi_max = max(phi_max,circshift(phi,Up));
phi_max = max(phi_max,circshift(phi,Down));

phi_min = phi;
phi_min = min(phi_min,circshift(phi,Right));
phi_min = min(phi_min,circshift(phi,Left));
phi_min = min(phi_min,circshift(phi,Up));
phi_min = min(phi_min,circshift(phi,Down));

d_Right =  grad_phi{1}*Deltax/2;
d_Left  = -d_Right;
d_Up    =  grad_phi{2}*Deltay/2;
d_Down  = -d_Up;

psi_ij = @(phi,grad_phi,d)     (phi_max-phi)./(d+(d==0)) .* (d>0) ...
                             + (phi_min-phi)./(d+(d==0)) .* (d<0) ...
                             +  1                        .* (d==0);

alpha = min( alpha , psi_ij(phi, grad_phi, d_Right) );
alpha = min( alpha , psi_ij(phi, grad_phi, d_Left ) );
alpha = min( alpha , psi_ij(phi, grad_phi, d_Up   ) );
alpha = min( alpha , psi_ij(phi, grad_phi, d_Down ) );

% alternate limiter
%alpha = min( alpha, max( 0, (circshift(phi,Right) - phi) ./ ( grad_phi{1}*Deltax/2 + (grad_phi{1}==0)).*(abs(grad_phi{1})>0)+ (grad_phi{1}==0)  ) );
%alpha = min( alpha, max( 0, (circshift(phi,Left)  - phi) ./ (-grad_phi{1}*Deltax/2 + (grad_phi{1}==0)).*(abs(grad_phi{1})>0)+ (grad_phi{1}==0)  ) );
%alpha = min( alpha, max( 0, (circshift(phi,Up)    - phi) ./ ( grad_phi{2}*Deltay/2 + (grad_phi{2}==0)).*(abs(grad_phi{2})>0)+ (grad_phi{2}==0)  ) );
%alpha = min( alpha, max( 0, (circshift(phi,Down)  - phi) ./ (-grad_phi{2}*Deltay/2 + (grad_phi{2}==0)).*(abs(grad_phi{2})>0)+ (grad_phi{2}==0)  ) );

grad_phi{1} = alpha .* grad_phi{1};
grad_phi{2} = alpha .* grad_phi{2};

end
