function plot_solution2D(var,var_name,clim,time,fig_num,pos_flag)
% plot the solution specified by solution_coeffs for 2D
Globals2D;

figure(fig_num);
imagesc([0,BoxSizeX],[0,BoxSizeY],var');
pbaspect([BoxSizeX,BoxSizeY,1]);
switch pos_flag
    case 'A'
        set(gcf,'position',[50,70+(fig_num-1)*220,BoxSizeX*200,BoxSizeY*200+20]);
    case 'B'
        set(gcf,'position',[20,70+(fig_num-1)*40,BoxSizeX*400,BoxSizeY*400+50]);
end
set(gca,'YDir','normal');
caxis(clim);
colormap(hot(256));
colorbar;
title([var_name, ' at t=',  num2str(time)],'interpreter','latex','fontsize',14);
%set(gca, 'XTick', []);
%set(gca, 'YTick', []);

end