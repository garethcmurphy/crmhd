function Pc_new = SolveAdvectionDiffusion(Pc,vx,vy,vx_new,vy_new,delta_t)
% solve the advection diffusion equation via implicit 2nd order
% Crank-Nicolson scheme

Globals2D;

% prev timestep flux
f = -(circshift(Pc.*vx,Right)-circshift(Pc.*vx,Left))/(2*Deltax) ...
    -(circshift(Pc.*vy,Up)-circshift(Pc.*vy,Down))/(2*Deltay) ...
    +D*(circshift(Pc,Right)-2*Pc+circshift(Pc,Left))/(Deltax^2) ...
    +D*(circshift(Pc,Up)-2*Pc+circshift(Pc,Down))/(Deltay^2) ;
f = f(:);

% get neighbor state velocities
vx_new_R = circshift(vx_new,Right);
vx_new_L = circshift(vx_new,Left);
vy_new_U = circshift(vy_new,Up);
vy_new_D = circshift(vy_new,Down);

% outflow BCs on left/right
vx_new_R(Nx,:) = vx_new(Nx,:);
vx_new_L(1,:)  = vx_new(1,:);


% convert states to row vectors
Pc = Pc(:);
vx_new_R = vx_new_R(:);
vx_new_L = vx_new_L(:);
vy_new_U = vy_new_U(:);
vy_new_D = vy_new_D(:);

% get indices of neighbors for constructing space matrix A
Ntot = Nx*Ny;
idx = reshape(1:Ntot,Nx,Ny);
idx_R = circshift(idx,Right);
idx_L = circshift(idx,Left);
idx_U = circshift(idx,Up);
idx_D = circshift(idx,Down);
idx = idx(:);
idx_R = idx_R(:);
idx_L = idx_L(:);
idx_U = idx_U(:);
idx_D = idx_D(:);

% construct A
e = ones(size(idx));
A = sparse(idx,idx,e) ...
    - 0.5*delta_t*sparse(idx,idx, e*(-2*D/Deltax^2 - 2*D/Deltay^2) ) ...
    - 0.5*delta_t*sparse(idx,idx_R,-vx_new_R/(2*Deltax) + D/Deltax^2 ) ...
    - 0.5*delta_t*sparse(idx,idx_L, vx_new_L/(2*Deltax) + D/Deltax^2 ) ...
    - 0.5*delta_t*sparse(idx,idx_U,-vy_new_U/(2*Deltay) + D/Deltay^2 ) ...
    - 0.5*delta_t*sparse(idx,idx_D, vy_new_D/(2*Deltay) + D/Deltay^2 );

% construct b
b = Pc + 0.5*delta_t*f;

% solve A Pc_new = b
Pc_new = A\b;

Pc_new = reshape(Pc_new,Nx,Ny);

end