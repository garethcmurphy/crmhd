function [Pf, k, total_power] = radial_power_spectrum(data_cube,BoxSize)
% computes radially averaged power spectrum of data_cube (2D or 3D)
% (data must be a cube)!

%% image size
dim = length(size(data_cube));
if dim == 2
    [sx, sy] = size(data_cube);
else
    [sx, sy, sz] = size(data_cube);
end

%% Compute power spectrum
data_cube_ft = fftshift(fftn(data_cube));
total_power = sum(sum(sum( abs(data_cube_ft).^2 /sx^dim )));
data_cube_ftn = (abs(data_cube_ft)).^2 * (2*pi/BoxSize)^dim;

% only consider one half of spectrum (due to symmetry)
half_dim = floor(sx/2) + 1;

%% Compute radially-averaged power spectrum
% make Cartesian grid, convert to polar
cartesian_pts = -sx/2:sx/2-1;
if dim == 2
    [X, Y] = meshgrid(cartesian_pts,cartesian_pts);  
    [theta, rho] = cart2pol(X, Y);
else
    [X, Y, Z] = meshgrid(cartesian_pts,cartesian_pts,cartesian_pts); 
    [azimuth,elevation,rho] = cart2sph(X,Y,Z);
end

rho = round(rho);   % (k-space)
idx = cell(half_dim, 1);
for r = 0:half_dim-1
    idx{r + 1} = find(rho == r);
end
Pf = zeros(1, half_dim);
for r = 0:half_dim-1
    Pf(1, r + 1) = nanmean( data_cube_ftn( idx{r+1} ) );
end

k = (1:half_dim) * 2*pi / (BoxSize);

% add geometrical factor
if dim == 2
    Pf = Pf*2*pi.*k;
else
    Pf = Pf*4*pi.*k.^2;
end
